
#ifndef AUTH_H
#define AUTH_H

#include "platform.h"

int calculateValue(const unsigned char key[20], BNUINT64 time_30sec_local_endian);

#endif