// standard headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "platform.h"
#include "auth.h"
#include "sha1.h"
#include "hmacsha1.h"

int calculateValue(const unsigned char key[20], BNUINT64 time_30sec_local_endian)
{
  HmacSha1Context hmctx;
  unsigned int startPos;

  unsigned char in[sizeof(BNUINT64)];
  unsigned char out[SHA1_OUTPUT_LEN];
  BNUINT32 value;

  #ifdef WINDOWS
    endian_swap64(&time_30sec_local_endian);
  #endif
  #ifdef UNIX
    time_30sec_local_endian = htobe64(time_30sec_local_endian);
  #endif

  if (hmacSha1InitContext(&hmctx, key, 20)) return -1;
  memcpy(in, &time_30sec_local_endian, sizeof(BNUINT64));
  if (hmacSha1(&hmctx, in, sizeof(BNUINT64), out)) return -2;

  startPos = out[19] & 0x0F;
  memcpy(&value, out + startPos, sizeof(BNUINT32));

  #ifdef UNIX
    value = be32toh(value);
  #endif
  #ifdef WINDOWS
    endian_swap32(&value);
  #endif

  value &= 0x7fffffff; // Why?????
  value %= 100000000;
  return value;
}

