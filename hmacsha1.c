
#include <stdio.h>
#include <string.h>

#include "sha1.h"
#include "hmacsha1.h"

void getResult(SHA1Context *sha1ctx, unsigned char *result);

void getResult(SHA1Context *sha1ctx, unsigned char *result)
{
	unsigned char *digest = (unsigned char *)(sha1ctx->Message_Digest);
	
	result[0]  = digest[3];
	result[1]  = digest[2]; 
	result[2]  = digest[1];
	result[3]  = digest[0];
	result[4]  = digest[7];
	result[5]  = digest[6];
	result[6]  = digest[5];
	result[7]  = digest[4];
	result[8]  = digest[11];
	result[9]  = digest[10];
	result[10] = digest[9]; 
	result[11] = digest[8];
	result[12] = digest[15];
	result[13] = digest[14];
	result[14] = digest[13];
	result[15] = digest[12];
	result[16] = digest[19];
	result[17] = digest[18];
	result[18] = digest[17];
	result[19] = digest[16];
}

int hmacSha1InitContext(HmacSha1Context *ctx, unsigned char *key, unsigned int keyLen)
{
	unsigned int i;
	unsigned char keypad[SHA1_BLOCK_LEN];
	
	if (keyLen > SHA1_BLOCK_LEN) return -1;
	
	memcpy(keypad, key, keyLen);
	if (keyLen != SHA1_BLOCK_LEN) memset(keypad + keyLen, 0, SHA1_BLOCK_LEN - keyLen);
	
	for (i = 0; i < SHA1_BLOCK_LEN; i++) ctx->ikeypad[i] = keypad[i] ^ 0x36;
	for (i = 0; i < SHA1_BLOCK_LEN; i++) ctx->okeypad[i] = keypad[i] ^ 0x5C;
	
	return 0;
}

int hmacSha1(HmacSha1Context *ctx, unsigned char *in, unsigned int inLen, unsigned char out[SHA1_OUTPUT_LEN])
{
	unsigned char result1[SHA1_OUTPUT_LEN];
	SHA1Context sha1ctx;
	
	SHA1Reset(&sha1ctx);
	SHA1Input(&sha1ctx, ctx->ikeypad, SHA1_BLOCK_LEN);
	SHA1Input(&sha1ctx, in, inLen);
	if (!SHA1Result(&sha1ctx)) return -1;
	getResult(&sha1ctx, result1);
	
	SHA1Reset(&sha1ctx);
	SHA1Input(&sha1ctx, ctx->okeypad, SHA1_BLOCK_LEN);
	SHA1Input(&sha1ctx, result1, SHA1_OUTPUT_LEN);
	if (!SHA1Result(&sha1ctx)) return -1;
	getResult(&sha1ctx, out);
	
	return 0;
}
