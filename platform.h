#ifndef _BNETA_PLATFORM_H
#define _BNETA_PLATFORM_H
  
  #ifdef UNIX
  	#include <sys/types.h>
    #include <sys/time.h>
    #include <endian.h>
    #include <inttypes.h>
    
    #define BNUINT16 uint16_t
    #define BNUINT32 uint32_t
    #define BNUINT64 uint64_t
  #endif
  
  #ifdef WINDOWS
    #include <windows.h>
    #define BNUINT16 u_short
    #define BNUINT32 unsigned long
    #define BNUINT64 ULONGLONG
  #endif

#endif

