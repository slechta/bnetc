
#ifndef HMACSHA1_H
#define HMACSHA1_H

#define SHA1_OUTPUT_LEN 20
#define SHA1_BLOCK_LEN 64

typedef struct HMAC_CTX
{
    unsigned char ikeypad[SHA1_BLOCK_LEN];
	unsigned char okeypad[SHA1_BLOCK_LEN];
} HmacSha1Context;

int hmacSha1InitContext(HmacSha1Context *ctx, unsigned char *key, unsigned int keyLen);
int hmacSha1(HmacSha1Context *ctx, unsigned char *in, unsigned int inLen, unsigned char out[SHA1_OUTPUT_LEN]);

#endif
