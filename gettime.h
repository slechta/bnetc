#ifndef GETTIME_H
#define GETTIME_H

#include "platform.h"

void endian_swap16(BNUINT16 *x);
void endian_swap32(BNUINT32 *x);
void endian_swap64(BNUINT64 *x);

/* Return miliseconds sice UNIX epoch in local endianess */
void getmilis(BNUINT64 *msec);

#endif
