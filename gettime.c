#include <stdio.h>
#include <time.h>
#include <errno.h>

#ifdef LINUX
  #include <sys/time.h>
#endif

#include "platform.h"

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

#include "gettime.h"


void endian_swap16(BNUINT16 *x)
{
    *x = (*x>>8) | (*x<<8);
}

void endian_swap32(BNUINT32 *x)
{
    *x = (*x>>24) | ((*x<<8) & 0x00FF0000) | ((*x>>8) & 0x0000FF00) | (*x<<24);
}


void endian_swap64(BNUINT64 *x)
{
    *x = (*x>>56) |
        ((*x<<40) & 0x00FF000000000000) |
        ((*x<<24) & 0x0000FF0000000000) |
        ((*x<<8)  & 0x000000FF00000000) |
        ((*x>>8)  & 0x00000000FF000000) |
        ((*x>>24) & 0x0000000000FF0000) |
        ((*x>>40) & 0x000000000000FF00) |
        (*x<<56);
}

#ifdef WINDOWS
  // Returns void; msvc issue - cannot return ULONLONG - 32 bit truncate
  void getmilis(BNUINT64 *msec)
  {
    FILETIME ft;
    PULARGE_INTEGER lint;
    ULONGLONG time;

    GetSystemTimeAsFileTime(&ft);

    lint = (PULARGE_INTEGER) &ft;
    time = lint->QuadPart;

    time /= 10;
    time -= DELTA_EPOCH_IN_MICROSECS;
    time /= 1000;

    memcpy(msec, &time, 8);
  }
#endif

#ifdef UNIX
  void getmilis(BNUINT64 *msec)
  {
    struct timeval tv;

    if (gettimeofday(&tv, NULL))
    {
        perror("error - gettimeofday()");
	*msec = 0;
    }

    *msec = (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
  }
  
#endif
