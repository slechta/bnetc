
CC = gcc
CCFLAGS = -Wall -I. -O3 -ggdb -DUNIX

all: shatest sha bneta

shatest: shatest.o sha1.o
	$(CC) shatest.o sha1.o -o shatest
	
sha: sha.o sha1.o
	$(CC) sha.o sha1.o -o sha
	
bneta: sha.o sha1.o bneta.o hmacsha1.o gettime.o auth.o
	$(CC) bneta.o sha1.o hmacsha1.o gettime.o auth.o -o bneta
	
#tells how to make an *.o object file from an *.c file
%.o: %.c
	$(CC) -c $(CCFLAGS) $< -o $@
	
	
clean::
	rm -f *.o
	rm -f shatest
	rm -f sha
	
