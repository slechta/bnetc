// standard headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "platform.h"
#include "gettime.h"
#include "auth.h"

unsigned char key[20] = {0xAA, 0x86, 0x1B, 0x96, 0x4E, 0x97, 0x07, 0xCD, 0x1D, 0xF6, 0x22, 0x43, 0xDD, 0x18, 0xFF, 0xC2, 0xD5, 0x7C, 0xA5, 0x89};

int main(int argc, char *argv[])
{
  int value;
  BNUINT64 time_msec;
  getmilis(&time_msec);
  value = calculateValue(key, time_msec / 30000);
  printf("Value: %d\n", value);
}

/*
int main(int argc, char **argv)
{
  HmacSha1Context hmctx;
  unsigned int startPos;

  BNUINT64 time_msec;
  unsigned char in[sizeof(BNUINT64)];
  unsigned char out[SHA1_OUTPUT_LEN];
  BNUINT32 value;

  getmilis(&time_msec);

  // printf("time_msec:");
  // show_mem_rep((unsigned char *)&time_msec, 8);

  #ifdef WINDOWS
    // printf("time_msec: %I64d\n", time_msec);
  #endif

  time_msec /= 30000;

  #ifdef WINDOWS
    endian_swap64(&time_msec);
    // printArray("after swap:", (unsigned char*) &time_msec, 8);
  #endif
  #ifdef UNIX
    time_msec = htobe64(time_msec);
  #endif

  hmacSha1InitContext(&hmctx, key, sizeof(key));
  memcpy(in, &time_msec, sizeof(BNUINT64));
  // printArray("in", in, 8);
  hmacSha1(&hmctx, in, sizeof(BNUINT64), out);

  // printArray("out:", out, SHA1_OUTPUT_LEN);
  startPos = out[19] & 0x0F;
  // printf("starPos: %d\n", startPos);
  memcpy(&value, out + startPos, sizeof(BNUINT32));
  // printArray("value:", (unsigned char *)&value, sizeof(BNUINT32));

  #ifdef UNIX
    value = be32toh(value);
  #endif
  #ifdef WINDOWS
    endian_swap32(&value);
  #endif

  value &= 0x7fffffff; // Why?????
  value %= 100000000;
  printf("Value: %d\n", value);
  return 0;
}

*/
